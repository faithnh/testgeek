/*
 * Single Window Application Template:
 * A basic starting point for your application.  Mostly a blank canvas.
 * 
 * In app.js, we generally take care of a few things:
 * - Bootstrap the application with any data we need
 * - Check for dependencies like device type, platform version or network connection
 * - Require and open our top-level UI component
 *  
 */

//bootstrap and check dependencies
if (Ti.version < 1.8 ) {
	alert('Sorry - this application template requires Titanium Mobile SDK 1.8 or later');
}
else if (Ti.Platform.osname === 'mobileweb') {
	alert('Mobile web is not yet supported by this template');
}
else {
	//require and open top level UI component
	var TweetClient = require('util/tweet_client');
	var ApplicationWindow = require('ui/ApplicationWindow');
    //donayama, astronaughts
    var tweetClient = new TweetClient('donayama');
    new ApplicationWindow().open();
}
/**
 * ここで実行後のデータが渡される
 * result_geekwords:ギークワードの情報(高いものから出力)
 * 例：[{word:ゲシュタルト,count:2},{word:プログラム,count:1}]
 * flag:フラグ 0:小ギーク１：中ギーク２：大ギーク
 * result_geekword_percentage:ギークのパーセンテージ
*/
Ti.App.addEventListener("ExitTweetData", function(e){
	//ギークカウント
	alert(e.result_geekword_percentage+"%\n"+"ギーク判定"+e.flag);
	alert();
});
